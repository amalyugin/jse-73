package ru.t1.malyugin.tm.listener.scheme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeUpdateRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class SchemeUpdateListener extends AbstractSchemeListener {

    @NotNull
    private static final String NAME = "scheme-update";

    @NotNull
    private static final String DESCRIPTION = "Update scheme";

    @Nullable
    @Override
    public String getArgument() {
        return "-su";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@schemeUpdateListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull ConsoleEvent event) {
        System.out.println("[SCHEME UPDATE]");
        System.out.println("ENTER INIT TOKEN: ");
        @NotNull final String initToken = TerminalUtil.nextLine();
        @NotNull final SchemeUpdateRequest request = new SchemeUpdateRequest(initToken);
        schemeEndpoint.updateScheme(request);
    }

}