package ru.t1.malyugin.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.malyugin.tm.api.endpoint.IAuthSoapEndpoint;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.model.Result;
import ru.t1.malyugin.tm.model.User;

import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.IAuthSoapEndpoint")
public class AuthSoapEndpoint implements IAuthSoapEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @Override
    public Result login(
            final String username,
            final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception e) {
            e.printStackTrace();
            return new Result(e);
        }
    }

    @Override
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

    @Override
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        return userService.findByLogin(authentication.getName()).orElse(null);
    }

}