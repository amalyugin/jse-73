package ru.t1.malyugin.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.IUserRestEndpoint;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.model.User;

import java.util.Collection;

@RestController
@RequestMapping("/api/user")
public class UserRestEndpoint implements IUserRestEndpoint {

    @Autowired
    private IUserService userService;

    @Override
    @PostMapping("/create")
    public void create(
            @RequestBody final User user
    ) {
        userService.create(user);
    }

    @Override
    @PutMapping("/edit")
    public void edit(
            @RequestBody final User user
    ) {
        userService.edit(user);
    }

    @Override
    @GetMapping("/get/{id}")
    public User find(
            @PathVariable("id") final String id
    ) {
        return userService.findById(id).orElse(null);
    }

    @Override
    @GetMapping("/get")
    public Collection<User> getList() {
        return userService.findAll();
    }

}