package ru.t1.malyugin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.CustomUser;
import ru.t1.malyugin.tm.model.Project;

import java.util.Collection;

@Controller
public class ProjectController {

    private final static String REDIRECT_TO_LIST = "redirect:/projects";

    @Autowired
    private IProjectService projectService;

    @GetMapping("/projects")
    public String showProjectList(
            @AuthenticationPrincipal final CustomUser user,
            final Model model
    ) {
        Collection<Project> projects = projectService.findAllForUser(user.getUserId());
        model.addAttribute("projects", projects);
        return "list/projectList";
    }

    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        projectService.createForUser(user.getUserId());
        return REDIRECT_TO_LIST;
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        projectService.deleteByIdForUser(user.getUserId(), id);
        return REDIRECT_TO_LIST;
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("project") Project project
    ) {
        projectService.editForUser(user.getUserId(), project);
        return REDIRECT_TO_LIST;
    }

    @GetMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id,
            final Model model
    ) {
        final Project project = projectService.findByIdForUser(user.getUserId(), id);
        model.addAttribute("project", project);

        final Status[] statuses = Status.values();
        model.addAttribute("statuses", statuses);

        return "/edit/projectEdit";
    }

}