package ru.t1.malyugin.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_user")
public class User {

    private static final long serialVersionUID = 1;

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "login")
    @NotBlank(message = "Login cannot be blank")
    private String login;

    @Column(name = "password")
    @NotBlank(message = "Password cannot be blank")
    private String passwordHash;

    @JsonIgnoreProperties("user")
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

}