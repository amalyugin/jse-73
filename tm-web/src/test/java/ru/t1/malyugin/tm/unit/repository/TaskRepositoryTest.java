package ru.t1.malyugin.tm.unit.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.repository.TaskRepository;

import java.util.UUID;

@DataJpaTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private static final String USER_ID = UUID.randomUUID().toString();
    private final Task task1 = new Task("T1", "T1");
    private final Task task2 = new Task("T2", "T2");
    private final Task task3 = new Task("T3", "T3");

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Before
    public void initTest() {
        task1.setUserId(USER_ID);
        task2.setUserId(USER_ID);
        task3.setUserId(USER_ID);

        entityManager.persist(task1);
        entityManager.persist(task2);
    }

    @After
    public void clean() {
        taskRepository.deleteByUserId(USER_ID);
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, taskRepository.findAllByUserId(USER_ID).size());
    }

    @Test
    public void findByIdTest() {
        final Task task = taskRepository.findByUserIdAndId(USER_ID, task1.getId()).orElse(null);
        Assert.assertEquals(task1.getId(), task.getId());
        Assert.assertEquals(task1.getName(), task.getName());
        Assert.assertEquals(task1.getDescription(), task.getDescription());
        Assert.assertEquals(task1.getStatus(), task.getStatus());
    }

    @Test
    public void deleteAllByUserIdTest() {
        taskRepository.deleteAllByUserId(USER_ID);
        Assert.assertEquals(0, taskRepository.countByUserId(USER_ID));
    }

    @Test
    public void deleteByIdTest() {
        Assert.assertNotNull(taskRepository.findByUserIdAndId(USER_ID, task1.getId()).orElse(null));
        taskRepository.deleteByUserIdAndId(USER_ID, task1.getId());
        Assert.assertNull(taskRepository.findByUserIdAndId(USER_ID, task1.getId()).orElse(null));
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(taskRepository.existsByUserIdAndId(USER_ID, task1.getId()));
        Assert.assertFalse(taskRepository.existsByUserIdAndId(USER_ID, task3.getId()));
    }

    @Test
    public void countByUserTest() {
        Assert.assertEquals(2, taskRepository.countByUserId(USER_ID));
    }

}