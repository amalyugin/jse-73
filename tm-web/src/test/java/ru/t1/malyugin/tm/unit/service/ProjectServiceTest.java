package ru.t1.malyugin.tm.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;

import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectServiceTest {

    private static final String USER_ID = UUID.randomUUID().toString();

    private final Project project1 = new Project("P1", "P1");

    private final Project project2 = new Project("P2", "P2");

    @Autowired
    private IProjectService projectService;

    @Before
    public void initTest() {
        projectService.addForUser(USER_ID, project1);
        projectService.addForUser(USER_ID, project2);
    }

    @After
    public void clean() {
        projectService.clearForUser(USER_ID);
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectService.countForUser(USER_ID));
    }

    @Test
    public void findByIdTest() {
        final Project project = projectService.findByIdForUser(USER_ID, project1.getId());
        Assert.assertEquals(project1.getId(), project.getId());
        Assert.assertEquals(project1.getName(), project.getName());
        Assert.assertEquals(project1.getDescription(), project.getDescription());
        Assert.assertEquals(project1.getStatus(), project.getStatus());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, projectService.countForUser(USER_ID));
    }

    @Test
    public void deleteAll() {
        projectService.clearForUser(USER_ID);
        Assert.assertEquals(0, projectService.countForUser(USER_ID));
    }

    public void deleteById() {
        Assert.assertNotNull(projectService.findByIdForUser(USER_ID, project1.getId()));
        projectService.deleteByIdForUser(USER_ID, project1.getId());
        Assert.assertNull(projectService.findByIdForUser(USER_ID, project1.getId()));
    }

    @Test
    public void editTest() {
        final Project project = projectService.findByIdForUser(USER_ID, project1.getId());
        project.setDescription("NEW D");
        project.setName("NEW N");
        projectService.editForUser(USER_ID, project);
        final Project newProject = projectService.findByIdForUser(USER_ID, project1.getId());
        Assert.assertEquals(project.getId(), newProject.getId());
        Assert.assertEquals(project.getName(), "NEW N");
        Assert.assertEquals(project.getDescription(), "NEW D");
    }

}