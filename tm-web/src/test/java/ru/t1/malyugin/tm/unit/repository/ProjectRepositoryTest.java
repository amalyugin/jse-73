package ru.t1.malyugin.tm.unit.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.repository.ProjectRepository;

import java.util.UUID;

@DataJpaTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    private static final String USER_ID = UUID.randomUUID().toString();

    private final Project project1 = new Project("P1", "P1");
    private final Project project2 = new Project("P2", "P2");
    private final Project project3 = new Project("P3", "P3");

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Before
    public void initTest() {
        project1.setUserId(USER_ID);
        project2.setUserId(USER_ID);
        project3.setUserId(USER_ID);

        entityManager.persist(project1);
        entityManager.persist(project2);
    }

    @After
    public void clean() {
        projectRepository.deleteByUserId(USER_ID);
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectRepository.findAllByUserId(USER_ID).size());
    }

    @Test
    public void findByIdTest() {
        final Project project = projectRepository.findByUserIdAndId(USER_ID, project1.getId()).orElse(null);
        Assert.assertEquals(project1.getId(), project.getId());
        Assert.assertEquals(project1.getName(), project.getName());
        Assert.assertEquals(project1.getDescription(), project.getDescription());
        Assert.assertEquals(project1.getStatus(), project.getStatus());
    }

    @Test
    public void countByUserTest() {
        Assert.assertEquals(2, projectRepository.countByUserId(USER_ID));
    }

    @Test
    public void deleteAllByUserIdTest() {
        projectRepository.deleteAllByUserId(USER_ID);
        Assert.assertEquals(0, projectRepository.countByUserId(USER_ID));
    }

    @Test
    public void deleteByIdTest() {
        Assert.assertNotNull(projectRepository.findByUserIdAndId(USER_ID, project1.getId()).orElse(null));
        projectRepository.deleteByUserIdAndId(USER_ID, project1.getId());
        Assert.assertNull(projectRepository.findByUserIdAndId(USER_ID, project1.getId()).orElse(null));
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(projectRepository.existsByUserIdAndId(USER_ID, project1.getId()));
        Assert.assertFalse(projectRepository.existsByUserIdAndId(USER_ID, project3.getId()));
    }

}