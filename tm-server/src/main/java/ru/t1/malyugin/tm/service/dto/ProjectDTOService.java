package ru.t1.malyugin.tm.service.dto;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.dto.IProjectDTOService;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.NameEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.repository.dto.ProjectDTORepository;

import java.util.List;

@Service
public class ProjectDTOService extends AbstractWBSDTOService<ProjectDTO> implements IProjectDTOService {

    @NotNull
    private final ProjectDTORepository projectDTORepository;

    @Autowired
    public ProjectDTOService(@NotNull final ProjectDTORepository projectDTORepository) {
        this.projectDTORepository = projectDTORepository;
    }

    @NotNull
    @Override
    protected ProjectDTORepository getRepository() {
        return projectDTORepository;
    }

    @Override
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId.trim());
        project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        add(project);
        return project;
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId.trim(), id.trim());
        if (project == null) throw new ProjectNotFoundException();
        if (!StringUtils.isBlank(name)) project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        update(project);
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        return getRepository().countByUserId(userId.trim());
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        getRepository().deleteByUserId(userId.trim());
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return getRepository().findByUserIdAndId(userId.trim(), id.trim()).orElse(null);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        return getRepository().findAllByUserId(userId.trim());
    }

}